# README #

This is a Wrapper for Croppie, a JQuery based crop utility. Read more about at [http://codepen.io/csalazar/full/eZjMeB/﻿](http://codepen.io/csalazar/full/eZjMeB/﻿)

# Platform #

Packaged as a extension for Yii Framework 1.x. Easily portable to Yii 2. (i dont like yii 2, sorry).

# Usage in Yii 1.x #

Inside any view, invoke the widget:

```
#!php
  <body>
     <form bla..>
	<?php 
    	    $bg_url = "some picture url to be cropped";
	    $this->widget("ext.croppie.Croppie",array(
		"id"=>"croppie1",
		"picture"=>$bg_url,
		"vp_w"=>200,
		"vp_h"=>100,
		"save"=>"#savebutton",
		"saveto"=>"#datafield",
	)); ?>
        <input type='hidden' id='datafield' name='datafield' />
        <button type='submit' id='savebutton' name='submit' />
     </form>
  </body>

```
# Logic #

You provide a raw picture to be cropped, the user make usage of the ui provided by this widget. The user clicks the "save" button, the widget handle it and put the coordinates and zoom attributes into a hidden field, you save this data and attach it to the target image, later you can use this information to cut the image using the coordinates provided by this widget.
