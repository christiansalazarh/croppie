<?php
class Croppie extends CWidget {
	public $id='yiicroppie';
	public $picture;
	public $vp_w = 150;
	public $vp_h = 200;
	public $save;		 // button id designed to be used for savings. add #
	public $saveto;		 // id of text field, will receive coords and zoom
	public $type;

	public function run(){
		$this->prepareAssets();
		
		$divid = $this->id;
		$var = "_".$this->id;

		Yii::app()->getClientScript()->registerScript(
		$this->id."_croppie_script",
		"
			var $var = $('#{$divid}').croppie({
				viewport: {
					width: {$this->vp_w}, height: {$this->vp_h},
					type: '{$this->type}'
				}
			});
			console.log('reading settings from: ','{$this->saveto}');
			var _{$var}_rawdata = jQuery('{$this->saveto}').val();
			console.log('data is:',_{$var}_rawdata);
			if(_{$var}_rawdata){
				var {$var}_curdata = JSON.parse(_{$var}_rawdata);
				$var.croppie('bind', { 
					url: '{$this->picture}' , 
					points: {$var}_curdata.points,
					zoom: {$var}_curdata.zoom
				});
			}else{
				$var.croppie('bind', { 
					url: '{$this->picture}', 
				});
			}
			jQuery('{$this->save}').click(function(){
				console.log('croppie get', $var.croppie('get'));
				var data = JSON.stringify($var.croppie('get'));
				$('{$this->saveto}').val(data);
			});
			
			
		",CClientScript::POS_LOAD);
		echo "<div id='{$divid}' class='yiicroppie'></div>";
	}

	private function prepareAssets(){
		$localdir = rtrim(dirname(__FILE__),"/")."/assets/";	
		$assets = rtrim(Yii::app()->getAssetManager()->publish($localdir),"/");

		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');

		$cs->registerCssFile($assets."/croppie.css");
		$cs->registerScriptFile($assets."/croppie.js");
	}
}
